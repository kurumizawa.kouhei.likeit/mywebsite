package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.TodoDao;
import model.User;

/**
 * Servlet implementation class TodoAddServlet
 */
@WebServlet("/CreateServlet")
public class CreateServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public CreateServlet() {
    super();

  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    HttpSession session = request.getSession();
    User userInfo = (User) session.getAttribute("userInfo");

    if (userInfo == null) {

      response.sendRedirect("TodoLoginServlet");
      return;
    }


    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/create.jsp");
    dispatcher.forward(request, response);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    request.setCharacterEncoding("UTF-8");

    String title = request.getParameter("title");
    String userId = request.getParameter("user_id");

    TodoDao todoDao = new TodoDao();


    if (title.equals("")) {
      request.setAttribute("errMsg", "未入力があります");
      request.setAttribute("title", title);
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/create.jsp");
      dispatcher.forward(request, response);
      return;

    } else {
      todoDao.insert(title, userId);

      response.sendRedirect("ListServlet");
    }

  }

}
