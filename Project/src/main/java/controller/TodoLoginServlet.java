package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/TodoLoginServlet")
public class TodoLoginServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;


  public TodoLoginServlet() {
    super();
  }


  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
    dispatcher.forward(request, response);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    request.setCharacterEncoding("UTF-8");

    String email = request.getParameter("email");
    String password = request.getParameter("password");

    String encodepass = utill.EncodePassword.SecretPassword(password);

    UserDao userDao = new UserDao();
    User user = userDao.findByLoginInfo(email, encodepass);

    if (user == null) {
      // リクエストスコープにエラーメッセージをセット
      request.setAttribute("errMsg", "ログインIDまたはパスワードが異なります。");
      // 入力したログインIDを画面に表示するため、リクエストに値をセット
      request.setAttribute("email", email);

      // ログインjspにフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
      dispatcher.forward(request, response);
      return;
    }

    HttpSession session = request.getSession();
    session.setAttribute("userInfo", user);
    
    response.sendRedirect("ListServlet");


    }
  }


