package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.User;

public class UserDao {

  public User findByLoginInfo(String email, String password) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE email = ? and password = ?";

      System.out.println(email + " " + password);

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, email);
      pStmt.setString(2, password);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      String emailData = rs.getString("email");
      String name = rs.getString("name");
      String passwordData = rs.getString("password");

      return new User(id, emailData, name, passwordData);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }

  }

  public void insertuser(String email, String password, String name2) {

    Connection conn = null;

    try {

      conn = DBManager.getConnection();

      String sql =
          "INSERT INTO user(email,password,name) values(?,?,?)";


      System.out.println(email + " " + password + " " + name2);

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, email);
      pStmt.setString(2, password);
      pStmt.setString(3, name2);



      pStmt.executeUpdate();



    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();

        }
      }
    }

  }

}
