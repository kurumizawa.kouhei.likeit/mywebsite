package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dao.UserDao;

/**
 * Servlet implementation class AddServlet
 */
@WebServlet("/AddServlet")
public class AddServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public AddServlet() {
    super();


  }



  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {



    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Add.jsp");
    dispatcher.forward(request, response);

  }


  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    request.setCharacterEncoding("UTF-8");


    String email = request.getParameter("email");
    String password = request.getParameter("password");
    String passwordconfirm = request.getParameter("password-confirm");
    String name2 = request.getParameter("name");


    UserDao userDao = new UserDao();


    if (email.equals("") || !password.equals(passwordconfirm) || password.equals("")
        || name2.equals("")) {

      request.setAttribute("errMsg", "未入力があります");
      request.setAttribute("email", email);
      request.setAttribute("password", password);
      request.setAttribute("humanname", name2);

      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Add.jsp");
      dispatcher.forward(request, response);
      return;

    } else {

      String encodepass = utill.EncodePassword.SecretPassword(password);

      userDao.insertuser(email, encodepass, name2);

      response.sendRedirect("TodoLoginServlet");
    }


  }

}
