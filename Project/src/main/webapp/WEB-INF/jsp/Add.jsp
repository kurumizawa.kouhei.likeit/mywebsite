<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>TODO App | ユーザー新規登録</title>
<!-- BootstrapのCSS読み込み -->
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
	crossorigin="anonymous">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
	crossorigin="anonymous"></script>
<!-- FontAwesomeの読み込み-->
<script src="https://kit.fontawesome.com/f4ccc568fd.js"
	crossorigin="anonymous"></script>
</head>
<body class="bg-light">

	<!-- header -->
	<header>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-3">
			<div class="container">
				<a class="navbar-brand" href="ListServlet"> <!-- ナビゲーションバーの「TODOAPP」※ FontAwesome使いましたが深い意味ないです -->
					<i class="fa-solid fa-t"></i> <i class="fa-solid fa-o"></i> <i
					class="fa-solid fa-d"></i> <i class="fa-solid fa-o"></i> <i
					class="fa-solid fa-a"></i> <i class="fa-solid fa-p"></i> <i
					class="fa-solid fa-p"></i>
				</a>

				<div class="d-flex">
					<ul class="navbar-nav">
						
						<li class="nav-item"><a class="nav-link text-danger"
							href="LogoutServlet">ログアウト</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<!-- /header -->

	<!-- body -->
	<div class="container-fluid">
		<div class="row mb-3">
			<div class="col">
				<h1 class="text-center">ユーザ新規登録</h1>
			</div>
		</div>


		<div class="row">
			<div class="col-6 offset-3">
				<form action="AddServlet" method="POST">
					<c:if test="${errMsg != null}">
						<div class="alert alert-danger" role="alert">${errMsg}</div>
					</c:if>
					<div class="form-group row">
						<label for="user-id" class="control-label col-3">ログインメール</label>
						<div class="col-9">
							<input type="text" name="email" id="inputLoginId"
								class="form-control" value="${email}">
						</div>
					</div>
					<div class="form-group row">
						<label for="password" class="control-label col-3">パスワード</label>
						<div class="col-9">
							<input type="password" name="password" id="inputPassword"
								class="form-control" >
						</div>
					</div>
					<div class="form-group row">
						<label for="password-confirm" class="control-label col-3">パスワード(確認)</label>
						<div class="col-9">
							<input type="password" name="password-confirm"
								id="password-confirm" class="form-control">
						</div>
					</div>
					<div class="form-group row">
						<label for="user-name" class="control-label col-3">ユーザ名</label>
						<div class="col-9">
							<input type="text" name="name" id="user-name"
								class="form-control" value="${humanname}">
						</div>
					</div>


					<div>

						<button type="submit" value="登録"
							class="btn btn-primary btn-block form-submit">登録</button>
					</div>
				</form>



				<div class="row mt-3">
					<div class="col">
						<a href="TodoLoginServlet">戻る</a>
					</div>
				</div>
			</div>
		</div>
	</div>




</body>

</html>