package utill;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.xml.bind.DatatypeConverter;

public class EncodePassword {

  public static String SecretPassword(String secret) {

    Charset charset = StandardCharsets.UTF_8;

    String algorithm = "MD5";

    String encodeStr = "";

    try {
      byte[] bytes = MessageDigest.getInstance(algorithm).digest(secret.getBytes(charset));

      encodeStr = DatatypeConverter.printHexBinary(bytes);

      // 暗号化結果の出力
      System.out.println(encodeStr);

    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }


    return encodeStr;
  }

}
