package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Todo;


public class TodoDao {

  public List<Todo> findAll() {
    Connection conn = null;
    List<Todo> todoList = new ArrayList<Todo>();

    try {

      conn = DBManager.getConnection();

      // SELECT文を準備

      String sql = "SELECT * FROM todo ";

      // SELECTを実行し、結果表を取得
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);

      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int id = rs.getInt("id");
        String title = rs.getString("title");
        int user_id = rs.getInt("user_id");

        Todo todo = new Todo(id, title, user_id);

        todoList.add(todo);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return todoList;
  }



  public void delete(String id) {

    Connection conn = null;
    try {

      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = " DELETE FROM todo WHERE id = ? ";



      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);

      pStmt.setString(1, id);

      pStmt.executeUpdate();



    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();

        }
      }
    }


  }

  public void insert(String title, String user_id) {
    Connection conn = null;

    try {

      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "INSERT INTO todo(title,user_id) values(?,?) ";


      System.out.println(title);

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, title);
      pStmt.setString(2, user_id);



      pStmt.executeUpdate();



    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();

        }
      }
    }


  }

  public void update(String title, String user_id, String id) {
    Connection conn = null;
    try {

      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "UPDATE todo SET title = ?, user_id = ?  WHERE id = ? ";



      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, title);
      pStmt.setString(2, user_id);
      pStmt.setString(3, id);



      pStmt.executeUpdate();



    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();

        }
      }
    }
  }

  public List<Todo> Search(String title) {
    Connection conn = null;
    List<Todo> todoList = new ArrayList<Todo>();

    try {

      conn = DBManager.getConnection();

      StringBuilder sql = new StringBuilder("SELECT * FROM todo");

      System.out.println(title);
      ArrayList<String> sqlList = new ArrayList<String>();



      if (!title.equals("")) {

        sql.append(" WHERE title LIKE ? ");
        sqlList.add("%" + title + "%");

      }


      PreparedStatement pStmt = conn.prepareStatement(sql.toString());
      for (int i = 0; i < sqlList.size(); i++) {

        pStmt.setString(i + 1, sqlList.get(i));
      }



      ResultSet rs = pStmt.executeQuery();

      while (rs.next()) {
        int id = rs.getInt("id");
        String title1 = rs.getString("title");
        int user_id = rs.getInt("user_id");

        Todo todo = new Todo(id, title1, user_id);
        todoList.add(todo);
      }



    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return todoList;
  }



  public Todo findById(int id) {
    Connection conn = null;
    try {

      conn = DBManager.getConnection();

      String sql = "select * from todo where id = ? ";

      System.out.println(id);

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setLong(1, id);
      ResultSet rs = pStmt.executeQuery();

      rs.next();
      int id1 = rs.getInt("id");
      String title = rs.getString("title");
      int user_id = rs.getInt("user_id");
      return new Todo(id1, title, user_id);



    } catch (SQLException e) {

      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    return null;

  }
}

