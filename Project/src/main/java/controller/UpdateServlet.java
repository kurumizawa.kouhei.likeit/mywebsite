package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.TodoDao;
import model.Todo;
import model.User;

/**
 * Servlet implementation class TodoUpdateServlet
 */
@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UpdateServlet() {
    super();
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    HttpSession session = request.getSession();
    User userInfo = (User) session.getAttribute("userInfo");

    if (userInfo == null) {

      response.sendRedirect("TodoLoginServlet");
      return;
    }

    String idString = request.getParameter("id");


    int id = Integer.valueOf(idString);


    System.out.println(id);

    TodoDao todoDao = new TodoDao();
    Todo todo = todoDao.findById(id);

    request.setAttribute("todo", todo);


    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
    dispatcher.forward(request, response);

  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    request.setCharacterEncoding("UTF-8");



    String title = request.getParameter("title");
    String user_id = request.getParameter("userId");
    String id = request.getParameter("id");


    TodoDao todoDao = new TodoDao();


    if (title.equals("")) {

      request.setAttribute("errMsg", "未入力があります");
      request.setAttribute("title", title);

      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
      dispatcher.forward(request, response);

      return;

    } else {

      todoDao.update(title, user_id, id);


      response.sendRedirect("ListServlet");

    }
  }

}
